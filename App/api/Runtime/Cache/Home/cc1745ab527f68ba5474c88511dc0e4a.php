<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>标题</title>
</head>
<body style="text-align: center">
    <h1>接口:标题</h1>
   
    <div>
    	<h2>接口说明:</h2>
    	<span>接口说明的内容</span>
    </div>
    
    <div class="apiparam">
    	<h2>接口参数</h2>
    	<table border="1px" cellpadding="10px" style="margin-left: 550px;">
    		<tr>
    			<th>参数名称</th>
    			<th>类型</th>
    			<th>是否必须</th>
    			<th>默认值</th>
    			<th>其他</th>
    			<th>说明</th>
    		</tr>
    		
    		<tr>
    			<td>username</td>
    			<td>字符串</td>
    			<td>可选</td>
    			<td>默认值</td>
    			<td></td>
    			<td></td>
    		</tr>
    		
    	</table>
    </div>
   
    <div class="apiresult">
       <h2>返回结果</h2>
    	<table border="1px" cellpadding="10px" style="margin-left: 550px;">
    		<tr>
    			<th>返回字段</th>
    			<th>类型</th>
    			<th>说明</th>
    		</tr>
    		
    		<tr>
    			<td>title</td>
    			<td>字符串</td>
    			<td>标题</td>
    		</tr>
    		<tr>
    			<td>content</td>
    			<td>字符串</td>
    			<td>内容</td>
    		</tr>
    		<tr>
    			<td>version</td>
    			<td>字符串</td>
    			<td>版本，格式：X.X.X</td>
    		</tr>
    		<tr>
    			<td>time</td>
    			<td>整型</td>
    			<td>当前时间戳</td>
    		</tr>
    		
    	</table>
    </div>
   
    <div class="apirequest">
    	<h2>请求模拟</h2>
    		
    		<form>
             <table border="1px" cellpadding="10px" style="margin-left: 550px;">
             <tr>
             	<th>参数</th>
             	<th>是否必填</th>
             	<th>值</th>
             </tr>
             <tr>
             	<td>service</td>
             	<td>必须</td>
             	<td><input type="text" name="service" value="App.Site.Index"></td>
             </tr>
             <tr>
                <td>username</td>
             	<td>可选</td>
             	<td><input type="text" name="value" value="value"></td>
             </tr>
             </table>
             <br>
            <select name="way"><option value="post">POST</option>
            <option value="get">GET</option>
            </select>&nbsp;&nbsp;
            <input type="text" name="url" value="<?php echo C('URL');?>" size="30">&nbsp;&nbsp;<input type="submit" name="sub">
    		</form>
    		

    </div>

    <div class="apireply">
    	<p>111</p>
    </div>
</body>
</html>