<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo ($apiname); ?></title>
    <script type="text/javascript" src="<?php echo C('JS');?>"></script>
</head>
<body style="text-align: center">
    <h1>接口:<?php echo ($apiname); ?></h1>
   
    <div>
    	<h2>接口说明:</h2>
    	<span><?php echo ($apinote); ?></span>
    </div>
    <div class="apiparam">
    	<h2>接口参数</h2>
    	<table border="1px" width="500px" cellpadding="10px" style="margin-left: 350px;">
    		<tr>
    			<th>参数名称</th>
    			<th>类型</th>
    			<th>是否必须</th>
    			<th>默认值</th>
    			<th>其他</th>
    			<th>说明</th>
    		</tr>
    		
    		<tr>
    			<td>replycount</td>
    			<td>字符串</td>
    			<td>可选</td>
    			<td>默认值</td>
    			<td></td>
    			<td>获取回复数量</td>
    		</tr>
    		<tr>
    			<td>topicid</td>
    			<td>字符串</td>
    			<td>可选</td>
    			<td>默认值</td>
    			<td></td>
    			<td>主题id</td>
    		</tr>
    		<tr>
    			<td>userid</td>
    			<td>字符串</td>
    			<td>可选</td>
    			<td>默认值</td>
    			<td></td>
    			<td>用户id</td>
    		</tr>
    		
    	</table>
    </div>
   
    <div class="apiresult">
       <h2>返回结果</h2>
    	<table border="1px" width="500px"  cellpadding="10px" style="margin-left: 350px;">
    		<tr>
    			<th>返回字段</th>
    			<th>类型</th>
    			<th>说明</th>
    		</tr>
    		
    		<tr>
    			<td>topicid</td>
    			<td>字符串</td>
    			<td>主题id</td>
    		</tr>
    		<tr>
    			<td>replycontent</td>
    			<td>字符串</td>
    			<td>回复内容</td>
    		</tr>
    		<tr>
    			<td>userid</td>
    			<td>字符串</td>
    			<td>用户id</td>
    		</tr>
    		
    	</table>
    </div>
   
    <div class="apirequest">
    	<h2>请求模拟</h2>
    	
    <form method="post" name="gettopic" onsubmit="return getreply()">
             <table border="1px" width="500px"  cellpadding="10px" style="margin-left: 350px;">
             <tr>
             	<th>参数</th>
             	<th>是否必填</th>
             	<th>值</th>
             </tr>
	<tr>
    			<td>replycount</td>
    			<td>必填</td>
    			<td><input type="text" name="reply_Num" value="5" id="reply_Num"></td>
    			
    		</tr>
    		<tr>
    			<td>topicid</td>
    			<td>可选</td>
    			<td><input type="text" name="topic_id" id="topic_id"></td>
    			
    		</tr>
    		<tr>
    			<td>userid</td>
    			<td>可选</td>
    			<td><input type="text" name="user_id" id="user_id"></td>
    			
    		</tr>
    		</table>
             <br>
            <select name="way" id="way"><option value="post">POST</option>
            <option value="get">GET</option>
            </select>&nbsp;&nbsp;
            <input type="text" name="url" id="url" value="<?php echo ($cont); ?>/GetReplyProcess/replycount/5" size="30">&nbsp;&nbsp;<input type="submit" name="sub" id="sub">
    		</form>

    </div>

    
    <p id="reply">
    	111
    </p>
    
</body>
</html>

    <script type="text/javascript">
    $(document).ready(function(){
        $("#sub").click(function(){
        	var reply_num=$("#reply_Num").val();
        	var topic_id=$("#topic_id").val();
        	var user_id=$("#user_id").val();
        	var way=$("#way").val();
        	if(way=='post'){
        		$.ajax({
               url:"/index-api.php/Home/Api/GetReplyProcess",
               type:"post",
               data:{reply_num:reply_num,topic_id:topic_id,user_id:user_id,way:way},
               dataType:"json",
               error:function(XMLHttpRequest,textStatus,errorThrown){
               	alert(XMLHttpRequest.status);
               	alert(XMLHttpRequest.readyState);
               	alert(textStatus);

               	  // alert('Error loading XML document');
               },
               success:function(data){
               	 // var obj=eval('('+data+')');
               	 // if(obj.status==1){
               	 // 	alert(obj);
               	 // }
               	 
               	 
               	 $("#reply").html(data);

               }
        	});
        	return false;
        }else{
        	var url=$("#url").val();
        	
        	window.location.href=url;
        	return false;
        }
        });
    });
    
</script>