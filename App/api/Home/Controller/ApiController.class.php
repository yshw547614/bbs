<?php 
namespace Home\Controller;
use Think\Controller;
class ApiController extends Controller{
	public function GetTopic(){
		$apiname="获取论坛主题内容";
		$apinote="获取论坛主题接口服务，当需要获取时可执行";
		$action=__CONTROLLER__;
		
		$this->assign('cont',$action);
		$this->assign('apiname',$apiname);
		$this->assign('apinote',$apinote);
		
		$this->display();
	}

	public function GetReply(){
		$apiname="获取论坛回帖内容";
		$apinote="获取论坛回帖接口服务，当需要获取时可执行";
		$action=__CONTROLLER__;
		
		$this->assign('cont',$action);
		$this->assign('apiname',$apiname);
		$this->assign('apinote',$apinote);
		
		$this->display();
		
	}

	public function GetTopicProcess(){
		$Topic=D('topic');
		$way=I('post.way');
		$controller=__CONTROLLER__;
		if($way=='post'){
           if(empty(I('post.topic_num'))){
           	$result=returnApiError('topiccount值为空');
           	$this->ajaxReturn($result);
           }else{
           	 $rows=$Topic->GetTopic(I('post.topic_num'),I('post.category_id'),I('post.user_id'));
           	 $result=returnApiSuccess('查询成功',$rows);
           	 $this->ajaxReturn($result);
           }
		}else{
			if(empty(I('get.topiccount'))){
			$result=returnApiError('topiccount值为空');
			$this->assign('result',$result);
           	$this->display();
			}else{
				$rows=$Topic->GetTopic(I('get.topiccount'),I('get.categoryid'),I('get.userid'));
           $result=returnApiSuccess('查询成功',$rows);
           $this->assign('result',$result);
           $this->display();
			}
		}

		// $Topic=D('topic');
		// $topic_num=I('post.topic_num');
		// $array=json_encode(array('data'=>$topic_num,'info'=>"文章数量",'status'=>1));
		
	}

	public function GetReplyProcess(){
		$Reply=D('reply');
		$way=I('post.way');
		$controller=__CONTROLLER__;
		if($way=='post'){
           if(empty(I('post.reply_num'))){
           	$result=returnApiError('replycount值为空');
           	$this->ajaxReturn($result);
           }else{
           	 $rows=$Reply->GetReply(I('post.reply_num'),I('post.topic_id'),I('post.user_id'));
           	 $result=returnApiSuccess('查询成功',$rows);
           	 $this->ajaxReturn($result);
           }
		}else{
			if(empty(I('get.replycount'))){
			$result=returnApiError('replycount值为空');
			$this->assign('result',$result);
           	$this->display();
			}else{
				$rows=$Reply->GetReply(I('get.replycount'),I('get.topicid'),I('get.userid'));
           $result=returnApiSuccess('查询成功',$rows);
           $this->assign('result',$result);
           $this->display();
			}
		}

		// $Topic=D('topic');
		// $topic_num=I('post.topic_num');
		// $array=json_encode(array('data'=>$topic_num,'info'=>"文章数量",'status'=>1));
		
	}
}


 ?>