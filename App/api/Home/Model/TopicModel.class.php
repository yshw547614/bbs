<?php 
namespace Home\Model;
use Think\Model;
class TopicModel extends \Think\Model{
   /**
    * 功能:主要用来对获取到的参数进行curd处理
    * @param $topic_num string default=5
    * @param $category_id string 主题板块
    * @param $user_id string 用户id
    * @return array $data
   **/ 
   public function GetTopic($topic_num=5,$category_id,$user_id){
   	  $Topic=M('topic');
   	  if($category_id==null && $user_id==null){
   	  	 $rows=$Topic->order('add_time desc')->field('title,content,category_id,add_user')->limit($topic_num)->select();
   	  	 return $rows;
   	  }elseif ($category_id !=null && $user_id !=null) {
   	  	$map=array(
   	  		'category_id'=>$category_id,
   	  		'add_user'=>$user_id
   	  		);
   	  	$rows=$Topic->field('title,content,category_id,add_user')->where($map)->limit($topic_num)->select();
   	  	return $rows;
   	  }elseif ($category_id !=null && $user_id==null) {
   	  	$map=array(
   	  		'category_id'=>$category_id
   	  		);
   	  	$rows=$Topic->field('title,content,category_id,add_user')->where($map)->limit($topic_num)->select();
   	  	return $rows;
   	  }elseif($category_id ==null && $user_id!=null){
        $map=array(
   	  		'add_user'=>$user_id
   	  		);
   	  	$rows=$Topic->field('title,content,category_id,add_user')->where($map)->limit($topic_num)->select();
   	  	return $rows;
   	  }
   	  
   }





}
?>