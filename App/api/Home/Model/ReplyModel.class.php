<?php 
namespace Home\Model;
use Think\Model;
class ReplyModel extends \Think\Model{
   /**
    * 功能:主要用来对获取到的参数进行curd处理
    * @param $reply_num string default=5
    * @param $topic_id string 主题板块
    * @param $user_id string 用户id
    * @return array $data
   **/ 
   public function GetReply($reply_num=5,$topic_id,$user_id){
   	  $Reply=M('reply');
   	  if($topic_id==null && $user_id==null){
   	  	 $rows=$Reply->order('add_time desc')->field('topic_id,reply_content,add_user')->limit($reply_num)->select();
   	  	 return $rows;
   	  }elseif ($topic_id !=null && $user_id !=null) {
   	  	$map=array(
   	  		'topic_id'=>$topic_id,
   	  		'add_user'=>$user_id
   	  		);
   	  	$rows=$Reply->field('topic_id,reply_content,add_user')->where($map)->limit($reply_num)->select();
   	  	return $rows;
   	  }elseif ($topic_id !=null && $user_id==null) {
   	  	$map=array(
   	  		'topic_id'=>$topic_id
   	  		);
   	  	$rows=$Reply->field('topic_id,reply_content,add_user')->where($map)->limit($reply_num)->select();
   	  	return $rows;
   	  }elseif($topic_id ==null && $user_id!=null){
        $map=array(
   	  		'add_user'=>$user_id
   	  		);
   	  	$rows=$Reply->field('topic_id,reply_content,add_user')->where($map)->limit($reply_num)->select();
   	  	return $rows;
   	  }
   	  
   }





}
?>