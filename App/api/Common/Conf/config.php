<?php
return array(
	'DB_TYPE'=>"mysql",
    'DB_HOST'  =>  'localhost', // 服务器地址
    'DB_NAME' =>  'bbs_demo',          // 数据库名
    'DB_USER'  =>  'root',      // 用户名
    'DB_PWD'   =>  '123456',          // 密码
    'DB_PREFIX' =>  'bbs_',    // 数据库表前缀
	'__API__'=>'Home/Api/',
	'URL'=>$_SERVER['SERVER_NAME'],
	'JS'=>'http://www.13.com/Public/Resource/Js/jquery.js',
	'CONTROLLER'=>__CONTROLLER__
);