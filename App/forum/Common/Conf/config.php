<?php
return array(
	'DB_TYPE'=>"mysql",
    'DB_HOST'  =>  'localhost', // 服务器地址
    'DB_NAME' =>  'bbs_demo',          // 数据库名
    'DB_USER'  =>  'root',      // 用户名
    'DB_PWD'   =>  '123456',          // 密码
    'DB_PREFIX' =>  'bbs_',    // 数据库表前缀
    'SHOW_PAGE_TRACE' =>true,
    'TMPL_PARSE_STRING'=>array(
        '__WEBSITE__'=>'http://www.13.com/index-forum.php',
        '__BBSPUBLIC__'=>'http://www.13.com/Public',
        '__PROJECTURL__'=>'http://www.13.com/index-forum.php/Home',
        '__ADMINURL__'=>'http://www.13.com/index-forum.php/Admin',
        '__WEBNAME__'=>'13论坛',
        '__BEIAN__'=>'',
        '__TONGJI__'=>''
        ),
    'URL_MODEL'=>4,
    'SITEINFO_CONFIG'=>array(
        'user_login_integral'=>1,
        'user_reg_stats'=>3,
        'user_reg_integral'=>10
        ),
    //邮件配置设置
    'SendMessage'=>array(
        "port"=>25,
        "host_name"=>"smtp.163.com",
        "time_out"=>120,
        "bodytype"=>"html",
        "user"=>"18820409315",
        "pass"=>"songfei123456",
        "from"=>"18820409315@163.com",
),
    //结尾
    //表单令牌
    'TOKEN_ON'=>true,
    'TOKEN_NAME'=>'__hash__',
    'TOKEN_TYPE'=>'md5',
    'TOKEN_RESET'=>true,
    //memcache缓存
    'DATA_CACHE_TYPE'=>"Memcache",
    'MEMCACHE_HOST'=>'tcp://127.0.0.1:11211',
    'DATA_CACHE_TIME'=>'3600',
    //网站是否关闭 0为关闭，1为开启
    'WEBOPEN'=>1,
)
?>