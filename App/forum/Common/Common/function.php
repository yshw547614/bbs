<?php 

/**
* 获取首页导航栏
**/

function Nav(){
	$Model=M("Category");
	$map=array(
		'category_level'=>'0',
		'parent_id'=>'0',
		);
	$row=$Model->where($map)->select();
	return $row;
}

/**
*获取某个导航栏id 对应的url
**/
function CategoryURL($id,$page=""){
      $config=C("TMPL_PARSE_STRING");
     IF(C("URL_MODEL")==4){
           //html伪静态
     	if(!$page){
     		$page=intval($page);
     		$url=$config['__WEBSITE__']."/forum-".$id."-{$page}.html";
     	}else{
     		$url=$config['__WEBSITE__']."/forum-".$id."-1.html";
     	}
     }else{
     	if(!$page){
     		$url=__APP__."?a=index&m=Category&id=".$id;
     	}else{
     		$url=__APP__."?a=index&m=Category&id=".$id."&p=".$page;
     	}
     }
     return $url;
}

/**
*获取category 中某个父类id的子类
*@param string $id 父类的id
**/

function GetSubCategory($id){
   $Model=M('Category');
   $map=array(
		'category_level'=>'1',
		'parent_id'=>$id,
		);
   $rows=$Model->where($map)->select();
   return $rows;
}

/**
*根据id获取板块的数据
*@param string $id 板块id
*@param string $field 获取的字段
**/
function CategoryData($id,$field=""){
	$Category=D('Category');
	$rows=$Category->getCategoryData($id);
    if(!empty($field)){
    	if(array_key_exists($field,$rows['newdata'][0])){
    		return $rows['newdata'][0][$field];
    	}else{
    		return $rows[$field];
    	}
    }else{
    return $rows;
  }
}

/**
*增加用户积分
*@param int $user  用户名称
*@param float $integral 增加的积分额度
*@param string $note 增加积分时写的备注 
**/

function AddUserIntegral($user,$integral,$note){
     if(!$integral){
     	return true;
     }
     $user_integral=M('user_integral');
     $integralData=array(
     	'user'=>$user,
     	'add_time'=>time(),
     	'integral'=>$integral,
     	'note'=>$note,
     	);
   if($add=$user_integral->add($integralData)){
   	return $add;
   }else{
   	Log::write("增加积分时，产生了错误:".$user_integral->getLastSql());
   	return false;
   }  
}

/**
*增加用户登陆次数
*@param  int $id 用户id
**/
function AddUserLoginNumber($id){
    $User=M("user");
    $map=array(
       'id'=>$id
    	);
    $User->where($map)->save(array("last_login_ip"=>get_client_ip()));
    $num=$User->where($map)->setInc('login_number',1);
    return $num;
}
/**
* 获取用户的头像
*@param $img string 用户头像
*@return $path 图片储存路径
**/

function HeadImg($img){
    $upload=new \Think\Upload();
    $upload->maxSize='2097152';
    $upload->exts=array('jpg','gif','png','jpeg');
    $upload->rootPath="/App/forum/Home/upload";
    $upload->savePath='/';
    $info=$upload->uploadOne($img);
    if(!$info){
      $error=$upload->getError();
      return $error;
    }else{
    $path='/App/forum/Home/upload'.$info['savepath'].$info['savename'];
    $name1=substr($info['savename'],0,-4).'thumb.jpg';
    $path2='/App/forum/Home/upload'.$info['savepath'].substr($info['savename'],0,-4).'thumb.jpg';
    $image= new \Think\Image();
    $image->open($path);
    $image->thumb(50,40)->save('/App/forum/Home/upload'.$info['savepath'].substr($info['savename'],0,-4).'thumb.jpg');
    }
    unlink($path);
    return $path2;
}
/**
* 模板中通过获取user_id获取对应的用户名称
*@param $id 用户id
*@return $name 用户名称
**/

 function GetUserName($id){
    $User=M('user');
    $map=array(
      'id'=>$id
      );
    $rows=$User->where($map)->find();
    return $rows['user_nickname'];
 }

 /**
* 根据主题的id获取该主题的回复数量
*@param $id string 主题的id
*@return 回复数量
 **/
 function GetReplyNum($id){
    $Reply=M('reply');
    $map=array(
      'topic_id'=>$id
      );
    $rows=$Reply->where($map)->count();
    return $rows;
 }

 /**
 *获取最后回复的用户名
 *@param $id string 主题id
 *@return 回复表中用户名
 **/
 function GetReplyName($id){
    $Reply=M('reply');
    $map=array(
      'topic_id'=>$id
      );
    $rows=$Reply->where($map)->order('add_time desc')->find();
    $add_user=$rows['add_user'];
    return GetUserName($add_user);

 }
 /**
*获取最后回复的发帖时间
*@param $id string 主题id
*@return 发帖的时间戳
 **/
function GetReplyTime($id){
   $Reply=M('reply');
    $map=array(
      'topic_id'=>$id
      );
    $rows=$Reply->where($map)->order('add_time desc')->find();
    if($rows['add_time'] !=''){
       return $rows['add_time'];
    }else{
      return '';
    }
    
}
/**
* 获取当前分类id的父级分类id
*@param $id int 当前分类id
*@return 父级分类id
**/
function GetParCategory($id){
   $Category=M('category');
   $map=array(
    'id'=>$id 
    );
   $rows=$Category->where($map)->find();
   $map2=array(
    'id'=>$rows['parent_id']
    );
   $rows2=$Category->where($map2)->find();
   return $rows2;
}

/**
*获取当前用户的id
*@param $user_email 当前用户cookie保存的email
*@return 当前用户的id
**/
function GetUser($user_email){
   $User=M('user');
   $map=array(
    'user_email'=>$user_email
    );
   $rows=$User->where($map)->find();
   return $rows;
}

/**
*获取当前分类的详情
*@param $category_id 当前分类的id
*@return 返回当前分类id的对象
**/
function GetCategory($id){
  $Category=M('category');
  $map=array(
    'id'=>$id
    );
  $rows=$Category->where($map)->find();
  return $rows;
}

/**
*通过用户email获取用户的积分
*@param $user_email 用户email
*@return 返回用户的积分
***/
function GetIntegral($user_email){
  $User=M('user');
  $map=array(
    'user_email'=>$user_email
    );
  $rows=$User->where($map)->find();
  $id=$rows['id'];
  $Integral=M('user_integral');
  $map=array(
    'user'=>$id 
    );
  $count=$Integral->where($map)->sum('integral');
  return $count;
}


 ?>