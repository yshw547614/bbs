<?php 
namespace Admin\Controller;
use Think\Controller;
Class NavController extends Controller{
       public function index(){
       	$Admin_category=D('admin_category');
		$rows=$Admin_category->GetNavList();
		$username=S('username');
		$nav_id=$rows[2]['id'];
		$Subnavlist=$Admin_category->GetSubNavList($nav_id);
		$this->assign('subnav',$Subnavlist);
        $this->assign('nav',$rows);
        $this->assign('username',$username);
		$this->display();
       }
}




?>