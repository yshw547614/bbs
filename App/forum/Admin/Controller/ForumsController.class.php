<?php 
namespace Admin\Controller;
use Think\Controller;
Class ForumsController extends Controller{
	public function index(){
		$Admin_category=D('admin_category');
		$rows=$Admin_category->GetNavList();
		$username=S('username');
		$nav_id=$rows[1]['id'];
		$Subnavlist=$Admin_category->GetSubNavList($nav_id);
		$this->assign('subnav',$Subnavlist);
        $this->assign('nav',$rows);
        $this->assign('username',$username);
		$this->display();
	}
}



?>