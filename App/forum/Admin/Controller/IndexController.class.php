<?php 
namespace Admin\Controller;
use Think\Controller;
Class IndexController extends Controller{
	public function verify(){
		$verify=new \Think\Verify();
		$verify->fontSize=10;
		$verify->length=4;
		$verify->useNoise=false;
		$verify->imageW=70;
		$verify->imageH=20;
        $verify->useNoise=false;
        $verify->expire=60;
        $verify->useCurve=false;
        $verify->fontttf='1.ttf';
		$verify->entry();
	} 
    public function check_verify($code,$id=""){
      $verify=new \Think\Verify();
      return $verify->check($code,$id);
    }
	public function login(){
     if($_POST){
       $user_email=I("post.user_email");
       $pwd=I("post.pwd");
       $verify=I("post.verifycode");
       $tokenName=I('post.__hash__');
       $Admin=M('admin');
       // $User=M('user');
       //   $map=array(
       //   	'user_email'=>$user_email
       //   	);
       //   $rows=$User->where($map)->join('bbs_admin ON bbs_user.id=bbs_admin.user_id')->select();
      if($this->check_verify($verify) && $Admin->autoCheckToken($_POST)){
         $User=M('user');
         $map=array(
         	'user_email'=>$user_email
         	);
         $rows=$User->where($map)->join('bbs_admin ON bbs_user.id=bbs_admin.user_id')->select();
         if($rows[0]['id']!=NULL){
         	$this->success('登陆成功','index',3);
            $userid=$rows[0]['id'];
            $username=$rows[0]['username'];
            S('userid',$userid,3600);
            S('username',$username,3600);
         }else{
         	$this->error('登陆失败','login');
         }
         
      }else{
      	$this->error('验证码错误或重复提交','login');
      }
  }
		$this->display();
	}

	public function index(){
		$Admin_category=D('admin_category');
        $rows=$Admin_category->GetNavList();
        // var_dump($rows[0]['id']);
        $username=S('username');
        $nav_id=$rows[0]['id'];
        // var_dump($nav_id);
        //获取服务器信息
        $SERVER_SOFTWARE=$_SERVER['SERVER_SOFTWARE'];
        // var_dump($nav_id);
        $Subnavlist=$Admin_category->GetSubNavList($nav_id);
        // var_dump($Subnavlist);
        // var_dump(S('3'));
        $Data=GetDataSize();
        $detail=GetCookieNav();
        // var_dump($detail);
        // $this->assign('subnav',$Subnavlist);
        // $this->assign('nav',$rows);
        $this->assign('username',$username);
        $this->assign('datasize',$Data);
		    $this->display();
	}
}


 ?>