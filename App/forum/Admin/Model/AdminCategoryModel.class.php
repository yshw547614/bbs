<?php 
namespace Admin\Model;
use Think\Model;
Class AdminCategoryModel extends Model{
    static $SubNav=array();

	/**
    *获取首页的导航内容
    *
	**/
	public function GetNavList(){
         $Admin_Category=M('admin_category');
         $rows=$Admin_Category->where('parent_id=0')->select();
         return $rows;
	}
   /**
   *获取当前选项卡的二级目录
   *@param $n 当前用户选择的导航选项卡ID
   *@return $rows 返回从memcache缓存中获取的值
   **/
	public function GetSubNavList($n){
		$Admin_Category=M('admin_category');
	   if(S("$n") != null){
	   	  return S("$n");
	   }else{
	   	 $map=array(
	   	 	'parent_id'=>$n
	   	 	);
	   	 $rows=$Admin_Category->where($map)->select();
	   	 if($n==1){
           return S('1',$rows,0,0);
	   	 }else{
            if(S("$n",$rows,0,0)){
	   	 	return S("$n");
	   	 }
	   	 }
	   }
	}
}



 ?>