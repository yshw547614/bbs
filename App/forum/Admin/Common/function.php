<?php 

/**获取数据库的大小
  *
  **/
function GetDataSize(){
	$Model=new \Think\Model();
    $sql="show table status from ".C('DB_NAME');
    $rows=$Model->query($sql);
    $size=0;
    foreach($rows as $value){
    	$size+=$value['data_length']+$value['index_length'];
    }
      $size=round(($size/1048576),2)."MB";
    return $size;

}

/**建立面包屑导航
**/
function GetCookieNav(){
  $url=$_SERVER['REQUEST_URI'];
  $array=explode('/',$url);
  $cont=(string)$array[3]."/"."index";
  $func=$array[4];
  $funcpath=$array[3]."/".$array[4];
  if($func=='index'){
     $map=array(
    'category_path'=>$cont
    );
    $Model=M('admin_category');
    $sql=$Model->where($map)->find();
  return $sql['category_title'];
  }else{
    $map=array(
    'category_path'=>$cont
    );
    $Model=M('admin_category');
    $sql=$Model->where($map)->find();

    $map1=array(
    'category_path'=>$funcpath
    );
    $Model=M('admin_category');
    $sql1=$Model->where($map1)->find();
  return $sql['category_title']."--".$sql1['category_title'];
  }
}

/**
*admin模块布局模板中获取主导航和次导航
*@param $id string 定义主导航次导航。主导航为0(默认),次导航为1 
*/
// function GetNavAdmin($id=0,$){
//    $Admin_category=M('admin_category');
//    if($id==0){
//       $rows=$Admin_category->field('id,category_title')->where('id<=12')->select();
//       return $rows;
//    }else{
//      if($id==1 && )
//    }
// }

/**
 * adminlayout中获取功能导航
 */
function GetNavAdmin(){
  $Admin_category=M('admin_category');
  $map=array(
    'parent_id'=>0
    );
  $rows=$Admin_category->where($map)->select();
  return $rows;
}

function GetAdminUser(){
  $adminname=S('username');
  return $adminname;
}

// function GetAdminCookie(){

// }
// 
function GetAdminSubNav(){
  $url=$_SERVER['REQUEST_URI'];
  $array=explode('/',$url);
  $cont=(string)$array[3]."/"."index";
  $func=$array[4];
  $funcpath=$array[3]."/".$array[4];
  $map=array(
    'category_path'=>$cont
    );
  $Model=M('admin_category');
  $rows=$Model->where($map)->find();
  $controller_id=$rows['id'];
  $map2=array(
    'parent_id'=>$controller_id
    );
  $rows2=$Model->where($map2)->select();
  return $rows2;
}

?>