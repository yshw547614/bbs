<?php
/**
 * @日期：2010年7月8日
 * @功能：ThinkPHP邮件发送扩展类库
 * @作者：李开涌
 *
 */
class SendMessage {
	//重要的选项
	static protected 	$smtp_port;
	static protected	$time_out;
	static protected	$host_name;
	static protected	$log_file;
	static protected	$relay_host;
	static public 		$debug;
	static protected	$auth;
	static protected	$user;
	static protected	$pass;
	static protected	$sock;
	static public 		$info;
	static protected    $from;
	//全部选项
	static protected 	$conf;
	/**
	 * 初始化类库
	 * Enter description here ...
	 * @param unknown_type $conf
	 */
	static public function init($conf=array()){
		if (!count($conf)){
			$conf=C("SendMessage");
		}
		if (!count($conf)){
			self::$info=json_encode(array(
				"stat"=>0,"info"=>"请提供配置数据","data"=>$conf,
			));
			return false;
		}
		if ($conf["type"]=="mail" || $conf["type"]==""){
			array_key_exists("port", $conf)?self::$smtp_port=$conf["port"]:self::$smtp_port=25;
			array_key_exists("host_name", $conf)?self::$host_name=$conf["host"]:self::$host_name="localhost";
			array_key_exists("time_out", $conf)?self::$time_out=$conf["time_out"]:120;
			array_key_exists("log_file", $conf["mail"])?self::$log_file=$conf["mail"]["log_file"]:self::$log_file=RUNTIME_PATH."Logs/mail.log";
			array_key_exists("relay_host", $conf["mail"])?self::$relay_host=$conf["mail"]["relay_host"]:self::$relay_host=self::$relay_host=$conf["host"];
			array_key_exists("auth", $conf["mail"])?self::$auth=$conf["mail"]["auth"]:self::$auth=true;
			array_key_exists("user", $conf)?self::$user=$conf["user"]:self::$user=null;
			array_key_exists("pass", $conf)?self::$pass=$conf["pass"]:self::$pass=null;
			array_key_exists("sock", $conf["mail"])?self::$sock=$conf["mail"]["sock"]:self::$sock=FALSE;
			array_key_exists("from", $conf["mail"])?self::$from=$conf["mail"]["from"]:self::$from=null;
			self::$debug=$conf;
			self::$conf=$conf;
		}
	}
	/**
	 * 发送邮件
	 * Enter description here ...
	 * @param string $body 邮件 内容
	 * @param string $to 目标地址
	 * @param unknown_type $cc
	 * @param unknown_type $bcc
	 * @param unknown_type $additional_headers
	 */
	static public  function SendMail($subject,$body,$to,$cc = "", $bcc = "", $additional_headers = ""){
		$conf=self::$conf;
		if ($conf["mail"]["phpmail"]){
			//使用php mail函数
			self::mailFun($to, $subject, $body);
			return true;
		}
		$from=self::$from;
		if (strlen($body)<2){
			self::$info=json_encode(array(
				"stat"=>0,"info"=>"请输入内容","data"=>$body,
			));
			return false;
		}		 	
		$mail_from = self::get_address(self::strip_comment($from));
		if (empty($mail_from)){
			self::$info=json_encode(array(
				"stat"=>0,"info"=>"没有发送方邮件地址","data"=>$mail_from,
			));
			return false;
		}	
		$header .= "MIME-Version:1.0\r\n";	
		$body = @ereg_replace("(^|(\r\n))(\.)", "\1.\3", $body);
		$filename=APP_PATH."Tpl/".$conf["mail"]["mail_templ"];	
		if (array_key_exists("mail_templ", $conf["mail"])){			
			if (!file_exists($filename)){
				self::$info=json_encode(array("stat"=>0,"info"=>"{$filename}文件不存在","data"=>$filename));
				return false;
			}					
			$html=file_get_contents($filename);
			$body=str_replace("{body}", $body, $html);
			$body=str_replace("{title}", $subject, $body);
			$body=str_replace("{time}", date("Y-m-d H:i:s",time()), $body);
			$body=str_replace("{form}", self::$from, $body);
			$header .= "Content-Type:text/html\r\n";
			
		}		
		if($conf["mail"]["bodytype"]=="HTML" || $conf["mail"]["bodytype"]=="html")
		{
			$header .= "Content-Type:text/html\r\n";
		}
		$header .= "To: ".$to."\r\n";
		if ($cc != "")
		{
			$header .= "Cc: ".$cc."\r\n";
		}
		$header .= "From: $from<".$from.">\r\n";
		$header .= "Subject: ".$subject."\r\n";
		$header .= $additional_headers;
		$header .= "Date: ".date("r")."\r\n";
		$header .= "X-Mailer:By Redhat (PHP/".phpversion().")\r\n";
		list($msec, $sec) = explode(" ", microtime());
		$header .= "Message-ID: <".date("YmdHis", $sec).".".($msec*1000000).".".$mail_from.">\r\n";
		$TO = explode(",", self::strip_comment($to));

		if ($cc != "")
		{
			$TO = array_merge($TO, explode(",", self::strip_comment($cc)));
		}

		if ($bcc != "")
		{
			$TO = array_merge($TO, explode(",", self::strip_comment($bcc)));
		}

		$sent = TRUE;
		foreach ($TO as $rcpt_to)
		{
			$rcpt_to = self::get_address($rcpt_to);
		if (!self::smtp_sockopen($rcpt_to))
		{
			self::log_write("Error: Cannot send email to ".$rcpt_to."\n");
			$sent = FALSE;
			continue;
		}
			if (self::smtp_send(self::$host_name, $mail_from, $rcpt_to, $header, $body))
		{
			self::log_write("E-mail has been sent to <".$rcpt_to.">\n");
		}else{
			self::log_write("Error: Cannot send email to <".$rcpt_to.">\n");
			$sent = FALSE;
		}
		fclose(self::$sock);
			self::log_write("Disconnected from remote host\n");
		}

	}
	static protected function smtp_send($helo, $from, $to, $header, $body = "") {
			if (! self::smtp_putcmd ( "HELO", $helo )) {
				return self::smtp_error ( "sending HELO command" );
			}
			#auth
			if (self::$auth) {
				if (! self::smtp_putcmd ( "AUTH LOGIN", base64_encode ( self::$user ) )) {
					return self::smtp_error ( "sending HELO command" );
				}
				
				if (! self::smtp_putcmd ( "", base64_encode ( self::$pass ) )) {
					return self::smtp_error ( "sending HELO command" );
				}
			}
			#
			if (! self::smtp_putcmd ( "MAIL", "FROM:<" . $from . ">" )) {
				return self::smtp_error ( "sending MAIL FROM command" );
			}
			
			if (! self::smtp_putcmd ( "RCPT", "TO:<" . $to . ">" )) {
				return self::smtp_error ( "sending RCPT TO command" );
			}
			
			if (! self::smtp_putcmd ( "DATA" )) {
				return self::smtp_error ( "sending DATA command" );
			}
			
			if (! self::smtp_message ( $header, $body )) {
				return self::smtp_error ( "sending message" );
			}
			
			if (! self::smtp_eom ()) {
				return self::smtp_error ( "sending <CR><LF>.<CR><LF> [EOM]" );
		}
		
		if (! self::smtp_putcmd ( "QUIT" )) {
			return self::smtp_error ( "sending QUIT command" );
		}
		self::$info=json_encode(array(
			"stat"=>1,"info"=>"邮件发送成功","data"=>$from,
		));
		return TRUE;
	}
	static  protected function smtp_sockopen($address) {
		if (self::$relay_host == "") {
			return self::smtp_sockopen_mx ( $address );
		} else {
			return self::smtp_sockopen_relay();
		}
	}
	static protected function smtp_sockopen_relay() {
		self::log_write ( "Trying to " . self::$relay_host . ":" . self::$smtp_port . "\n" );		
		self::$sock = @fsockopen ( self::$relay_host, self::$smtp_port, $errno, $errstr, self::$time_out );
		
		if (! (self::$sock && self::smtp_ok ())) {
			self::log_write ( "Error: Cannot connenct to relay host " .self::$relay_host . "\n" );
			self::log_write ( "Error: " . $errstr . " (" . $errno . ")\n" );
			return FALSE;
		}
		self::log_write ( "Connected to relay host " . self::$relay_host . "\n" );
		return TRUE;
		
	}
	static protected function smtp_sockopen_mx($address) {
		$domain = ereg_replace ( "^.+@([^@]+)$", "\1", $address );
		if (! @getmxrr ( $domain, $MXHOSTS )) {
			self::log_write ( "Error: Cannot resolve MX \"" . $domain . "\"\n" );
			return FALSE;
		}		
		foreach ( $MXHOSTS as $host ) {
			self::$log_write ( "Trying to " . $host . ":" . self::smtp_port . "\n" );
			self::$sock = @fsockopen ( $host, self::$smtp_port, $errno, $errstr, self::$time_out );
			if (! (self::$sock && self::smtp_ok ())) {
				self::log_write ( "Warning: Cannot connect to mx host " . $host . "\n" );
				self::log_write ( "Error: " . $errstr . " (" . $errno . ")\n" );
				continue;
			}
			self::log_write ( "Connected to mx host " . $host . "\n" );
			return TRUE;
		}
		self::log_write ( "Error: Cannot connect to any mx hosts (" . implode ( ", ", $MXHOSTS ) . ")\n" );
		return FALSE;
	}
	static protected function smtp_message($header, $body) {
		fputs ( self::$sock, $header . "\r\n" . $body );
		self::smtp_debug ( "> " . str_replace ( "\r\n", "\n" . "> ", $header . "\n> " . $body . "\n> " ) );
		
		return TRUE;
	}
	static protected function smtp_eom() {
		fputs ( self::$sock, "\r\n.\r\n" );
		self::smtp_debug ( ". [EOM]\n" );
		
		return self::smtp_ok ();
	}
	static protected function smtp_ok() {
		$response = str_replace ( "\r\n", "", fgets ( self::$sock, 512 ) );
		self::smtp_debug ( $response . "\n" );
		
		if (@! ereg ( "^[23]", $response )) {
			fputs ( self::$sock, "QUIT\r\n" );
			fgets ( self::$sock, 512 );
			self::log_write ( "Error: Remote host returned \"" . $response . "\"\n" );
			return FALSE;
		}
		return TRUE;
	}
	static protected function smtp_putcmd($cmd, $arg = "") {
		if ($arg != "") {
			if ($cmd == "")
				$cmd = $arg;
			else
				$cmd = $cmd . " " . $arg;
		}
		
		fputs ( self::$sock, $cmd . "\r\n" );
		self::smtp_debug ( "> " . $cmd . "\n" );
		
		return self::smtp_ok ();
	}
	static protected function smtp_error($string) {
		self::log_write ( "Error: Error occurred while " . $string . ".\n" );
		return FALSE;
	}
	static protected function log_write($message) {
		self::smtp_debug ( $message );
		
		if (self::$log_file == "") {
			return TRUE;
		}		
		$message = date ( "M d H:i:s " ) . get_current_user () . "[" . getmypid () . "]: " . $message;
		if (! @file_exists ( self::$log_file ) || ! ($fp = @fopen ( self::$log_file, "a" ))) {
			//self::smtp_debug ( "Warning: Cannot open log file \"" . self::$log_file . "\"\n" );
			touch(self::$log_file,time());
		}
		flock ( $fp, LOCK_EX );
		fputs ( $fp, $message );
		fclose ( $fp );		
		return TRUE;
	}
	static protected function strip_comment($address) {
		$comment = "\([^()]*\)";
		while ( @ereg ( $comment, $address ) ) {
			$address = ereg_replace ( $comment, "", $address );
		}		
		return $address;
	}
	static protected function get_address($address) {
		$address = @ereg_replace ( "([ \t\r\n])+", "", $address );
		$address = @ereg_replace ( "^.*<(.+)>.*$", "\1", $address );		
		return $address;
	}
	static protected function smtp_debug($message) {
		if (self::$debug) {
			echo $message;
		}
	}
	//使用mail函数发送
	static private function mailFun($posttu,$postsubject,$postcontent){
		try {
			$to = "{$posttu}";
	        $subject = "{$postsubject}";
	        $message = $postcontent;
	        $subject = "=?UTF-8?B?".base64_encode($subject)."?=";
	        $headers = "MIME-Version: 1.0" . "\r\n";
	        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	        return mail($to,$subject,$message,$headers);	
		}catch (Exception $e){			
			self::$debug=$e->getMessage();
			
		}
	}
	
}
?>
