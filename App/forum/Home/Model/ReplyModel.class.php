<?php 
namespace Home\Model;
use Think\Model;
class ReplyModel extends Model{
   
   /**
   *统计某个主题id的回帖数量
   *@param $id string 为主题id
   *@return 返回当前id下的回复的数量
   **/
   public function CountReply($id){
     $Reply=M('reply');
     $map=array(
     	'topic_id'=>$id
     	);
     $count=$Reply->where($map)->count('topic_id');
     return $count;
   }

   /**
   *获取某个主题id的回帖内容
   *@param $id string 主题id
   *@return 返回当前id下的回帖的数组
   **/
   public function GetReplyPage($id){
    $Reply=M('reply');
    $map=array(
      'topic_id'=>$id,
      'add_user'=>100
      );
    $rows=$Reply->where($map)->join('bbs_user ON bbs_reply.add_user=bbs_user.id')->select();
    return $rows;
   }

   /**
   *
   *
   *
   **/


}




 ?>