<?php 
namespace Home\Model;
use Think\Model;
class TopicModel extends \Think\Model{
	/**
	*统计某个小论坛模块的主题数量
	* @param $id  category_id 论坛的id
	* @param $time boolen 0/1 0代表统计总数，1代表统计今天总数
	**/
      public function CountTopic($id,$time){
           if($time==0){
            $Topic=M('topic');
            $map=array(
              'category_id'=>$id
            	);
            $rows=$Topic->where($map)->count();
            return $rows;
        }else{
        	$Topic=M('topic');
        	$timestamp0=strtotime(date('Y-m-d',time()));
        	$timestamp24=$timestamp0+86400;
        	$map=array(
              'category_id'=>$id,
              'add_time'=>array('between',array($timestamp0,$timestamp24))
        		);
        	$rows=$Topic->where($map)->count();
        	return $rows;
        }

      }
     /**
      * 获取分页类中对应id的用户名
      * @param $id array 
      * @return user_name
     **/
      public function get_user_name($list){
          $User=M('user');
          foreach ($list as $key => $value) {
          	foreach($value as $row =>$col){
          		$map=array(
          			'id'=>$col
          			);
          		$rows=$User->where($map)->find();
          	}
          }
      }

   public function GetTopic($id){
    $Topic=D('topic');
    $map=array(
      'id'=>$id
      );
    $rows=$Topic->where($map)->find();
    return $rows;
   }


}



 ?>