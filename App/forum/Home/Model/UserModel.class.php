<?php 
namespace Home\Model;
use Think\Model;
class UserModel extends Model{
	protected $_validate=array(
       array('user_email','email','请输入正确的email邮箱'),
       array('user_email','','Email已经存在',0,'unique',1),
       array('password','require','密码至少4位','1','4,20'),
       array('pwd_check','password','密码与之前不一致',0,'confirm'),
       array('verifynum','require','请输入验证码!'),
       array('user_nickname','require','请输入一个称呼!'),
       array('username','require','请输入一个用户名!')
		);

}


 ?>