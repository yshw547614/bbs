<?php 
namespace Home\Model;
class CategoryModel extends \Think\Model{
   protected $option;
   /**
   *根据目录id统计论坛不同板块的回帖数量，总的帖子数量，板块中主题数量，最新贴子内容
   *@param string $id category_id 论坛板块的id
   *@return array $data 以上内容
   **/
	public function getCategoryData($id=0,$data=array()){
       //统计板块的数据
		$topic=M('topic');
		//如果有$id那就统计id对应的板块的主题数量
		if($id>0){
        $map=array(
          'category_id'=>$id,
          'stats'=>1,
        	);
        $topicNum=$topic->where($map)->count();
        //如果没有$id，那就统计所有的板块的主题数量
		}else{
			$topicNum=$topic->count();
		}
		//统计回复的数量
	 $reply=M('reply'); 
	 $replyNum=$reply->where($map)->count();
	 //统计主题和回复的总数量
	 $topicAndreplyNum=$topicNum+$replyNum;
	 //当前板块最新的帖子
	$map1=array(
      'category_id'=>$id,
      'stats'=>1,
		);
      $newdata=$topic->order('add_time desc')->where($map1)->select();
    $data=array(
    	'topicNum'=>$topicNum,
    	'replyNum'=>$replyNum,
    	'topicAndreplyNum'=>$topicAndreplyNum,
    	'newdata'=>$newdata
    	);
    return $data;
	}

}



 ?>