<?php 
namespace Home\Controller;
use Think\Controller;
Class ContentController extends Controller{
	public function index(){
     $category_id=$_GET['id'];
     $topic_id=$_GET['p'];
     //获取当前分类的id和title
     $category_title=GetCategory($category_id)['category_title'];
     //获取用户id
     $user_email=S('user_email');
     $user_id=GetUser($user_email)['id'];
     $user_img=GetUser($user_email)['head_image'];
     $user_name=GetUser($user_email)['user_nickname'];
     $user_registertime=GetUser($user_email)['reg_time'];
     $user_lastlogintime=GetUser($user_email)['last_login_time'];
     //获取父级分类id和title
     $parent_title=GetParCategory($category_id)['category_title'];
     $parent_id=GetParCategory($category_id)['id'];
     //获取当前主题的标题
     $Topic=D('topic');
     $rows=$Topic->GetTopic($topic_id);
     $topic_title=$rows['title'];
     $topic_addtime=$rows['add_time'];
     $topic_content=$rows['content'];
      //获取发帖用户的积分
     $count=GetIntegral($user_email);
          //回复贴分页处理
     $Reply=D('reply');
     $reply_count=$Reply->CountReply($topic_id);
     $Page=new \Think\Page($reply_count,2);
     $page_count=($reply_count/2)+1;
     $map=array(
     	'topic_id'=>$topic_id
     	);
     $list=$Reply->where($map)->join('bbs_user ON bbs_reply.add_user=bbs_user.id')->page($_GET['y'].',2')->select();
     //前端模板赋值
     $this->assign('pageTitle',$topic_title);
     $this->assign('parent_title',$parent_title);
     $this->assign('parent_id',$parent_id);
     $this->assign('category_id',$category_id);
     $this->assign('category_title',$category_title);
     $this->assign('topic_id',$topic_id);
     $this->assign('user_id',$user_id);
     $this->assign('user_img',$user_img);
     $this->assign('user_name',$user_name);
     $this->assign('count',$count);//用户积分
     $this->assign('registertime',$user_registertime);
     $this->assign('lastlogintime',$user_lastlogintime);
     $this->assign('addtime',$topic_addtime);
     $this->assign('topic_content',$topic_content);
     //回复区域赋值
     $this->assign('reply_count',$reply_count);
     $this->assign('list',$list);
     $this->assign('page_count',$page_count);
		$this->display();
	}

   public function addreply(){
   	  $topic_id=$_GET['p'];
   	  $content=I('post.content');
   	  $content=htmlspecialchars_decode(html_entity_decode($content));
   	  $add_time=time();
   	  $category_id=$_GET['id'];
   	  $user_email=S('user_email');
      $add_user=GetUser($user_email)['id'];
      $user_img=GetUser($user_email)['head_image'];
      $user_name=GetUser($user_email)['user_nickname'];
      $user_id=GetUser($user_email)['id'];
      $count=GetIntegral($user_email);
      $user_registertime=GetUser($user_email)['reg_time'];
      $user_lastlogintime=GetUser($user_email)['last_login_time'];
      $add_ip=get_client_ip();
      $Reply=M('reply');
      $data=array( 
         'reply_content'=>$content,
         'topic_id'=>$topic_id,
         'category_id'=>$category_id,
         'add_time'=>$add_time,
         'add_user'=>$add_user,
         'add_ip'=>$add_ip,
         'stats'=>1
      	);
      $rows=$Reply->add($data);
      // $array=json_encode(array('info'=>"回复成功",'status'=>1));
   	  // 	$this->ajaxReturn($array);
      $ReplyDetail="
         <div style='margin-left: 10px;margin-top: 10px'>
    	<table style='width: 500px;min-height:100%;height:100%;' border='1px'>
    	<tr>
    		<td>
    			<div>
    				<span style='padding-left: 10px'>用户名:{$user_name}</span><br>
    				<br>
    				<span style='padding-left: 20px'><img src='$user_img'></span><br>
    				<br>
    				<span style='padding-left: 10px'>id:$user_id</span><br>
    				<br>
    				<span style='padding-left: 10px'>用户积分:$count</span><br>
    				<br>
    				<span style='padding-left: 10px'>注册时间:$user_registertime</span><br>
    				<br>
    				<span style='padding-left: 10px'>最后登录时间:$user_lastlogintime</span><br>
    			</div>
    		</td>
    		<td><div style='height:200px;overflow: auto; '><span>发布于:{$addtime}</span><br>
    		 {$content}</div></td>
    	</tr>
    	</table>
    </div>";
      // $rows=$Reply->data($data)->add();
   	  // if($rows !="" && $rows !=null){
   	  	$array=json_encode(array('detail'=>$ReplyDetail,'info'=>"回复成功",'status'=>1));
   	  	$this->ajaxReturn($array);
   	  // }else{
   	  // 	$array=json_encode(array('info'=>"回复失败",'status'=>0));
   	  // 	$this->ajaxReturn($array);
   	  // }
   	  // var_dump($content);
   	  // $this->display();
   }
}


 ?>