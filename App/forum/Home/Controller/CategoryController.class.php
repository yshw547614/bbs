<?php 
namespace Home\Controller;
use Think\Controller;
class CategoryController extends Controller{
	public function index(){
          $forum_id=$_GET['id'];
          $this->assign('forum_id',$forum_id);
		$Category=D("category");
		$map=array(
          'id'=>$_GET['id']
			);
		$rows=$Category->where($map)->find();
		$map1=array(
			'id'=>$rows['parent_id'],
			);
		$parent_rows=$Category->where($map1)->find();
		if($rows !="" && $parent_rows !=""){
		  $this->assign('title',$rows['category_title']);
		  $this->assign('pageTitle',$rows['category_title']);
          $this->assign('parent_title',$parent_rows['category_title']);
          $Topic=D("Topic");
          $topic_count=$Topic->CountTopic($_GET['id'],0);
          $today_topic_count=$Topic->CountTopic($_GET['id'],1);
          //实现主题分页
          $map=array(
          	'stats'=>1,
          	'category_id'=>$_GET['id']
          	);
          $list=$Topic->where($map)->order('add_time')->page($_GET['p'].',15')->select();
          $this->assign('list',$list);
          $count=$Topic->where($map)->count();
          $show=($count/15)+1;
          $this->assign('page',$show);
          $this->assign('today_topic_count',$today_topic_count);
          $this->assign('topic_count',$topic_count);
          $this->display();
       
		}else{
			$this->error('论坛访问错误','Index.php/Home/index');
		}
	}
}







 ?>