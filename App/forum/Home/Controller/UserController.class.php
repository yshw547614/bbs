<?php 
namespace Home\Controller;
use Think\Controller;
class UserController extends Controller{
	//验证码
	public function verify(){
		$verify=new \Think\Verify();
		$verify->fontSize=10;
		$verify->length=4;
		$verify->useNoise=false;
		$verify->imageW=70;
		$verify->imageH=20;
    $verify->useNoise=false;
    $verify->expire=60;
    $verify->useCurve=false;
    $verify->fontttf='1.ttf';
		$verify->entry();
	}
    
    public function check_verify($code,$id=""){
      $verify=new \Think\Verify();
      return $verify->check($code,$id);
    }

	public function userlogin(){

      if(!$this->check_verify($_POST["verification"])){
        $array=json_encode(array('data'=>$_POST['verification'],'info'=>"验证码出错",'status'=>0));
      	$this->ajaxReturn($array);
      }
      if(I('post.email','',FILTER_VALIDATE_EMAIL)==""){
        $array=json_encode(array('data'=>$_POST['email'],'info'=>"邮箱出错",'status'=>0));
      	$this->ajaxReturn($array);
      }

      if(I('post.password')==""||strlen($_POST['password'])<4){
        $array=json_encode(array('data'=>$_POST['password'],'info'=>"密码最少4位数",'status'=>0));
      	$this->ajaxReturn($array);
      }
     $User=D("user");
      $map=array(
           'user_email'=>I('post.email'),
           'password'=>md5(I('post.password')), 
      	   );
     $rows=$User->where($map)->find();
     
     if($rows['status']==1){
     	//登陆成功
     	// session('user_id',md5($rows['id'].I("email")));
      $user_id=md5($rows['id'].I("email"));
      S('user_id',$user_id);
     	// session('passowrd',md5($rows['password']));
      $password1=md5($rows['password']);
      S('password',$password1);
      $user_email=I('post.email');
      cookie('user_email',I('post.email'));
      S('user_email',$user_email);
     	//增加积分
      $config=C('SITEINFO_CONFIG');
      AddUserIntegral($rows['id'],$config['user_login_integral'],"登陆时增加的积分");
      //增加登陆次数
      AddUserLoginNumber($rows['id']); 
      $TMPL_PARSE_STRING=C('TMPL_PARSE_STRING');
      $url=$TMPL_PARSE_STRING["__PROJECTURL__"]."/user/userminicard";
      $array=json_encode(array('data'=>$url,'info'=>"登陆成功",'status'=>1));
      $this->ajaxReturn($array);
     }elseif($rows['status']==2){
      $array=json_encode(array('data'=>$_POST['email'],'info'=>"系统已经向{$row['user_email']}发送了验证邮件，请查收"));
     	$this->ajaxReturn($array);
     
     }elseif($rows['status']==3){
    $array=json_encode(array('data'=>$_POST['email'],'info'=>"你的用户资格管理员正在审核中"));
     	$this->ajaxReturn($array);
     }else{
      $array=json_encode(array('data'=>$_POST['email'],'info'=>"登录失败，请检查用户名或邮箱".$User->getLastSql(),'status'=>0));
     	$this->ajaxReturn($array);
     }
       

	}

	public function userminicard(){
    // $user_email=I('cookie.user_email');
    $user_email=S('user_email');
    $User=M('user');
    $map=array(
    'user_email'=>$user_email
      );
    $rows=$User->where($map)->find();

    $this->assign('user_name',$rows['username']);
    $this->assign('user_img',$rows['head_image']);
		$this->display();
	}

  public function register(){
    $pageTitle="用户注册";
    $this->assign('pageTitle',$pageTitle);
    if($this->userlogin){
      $this->success("你已成功登陆，无需注册");
    }
    $security=M('security_password');
    $rows=$security->select();
    $this->assign('security_list',$rows);
    $this->display();
  }

  public function registerpost(){
    $User=D("User");
    $data=$User->create();
    $user_info=array(
         'weibo'=>I($_POST['weibo']),
         'qq'=>I($_POST['qq']),
      );
    $path=HeadImg($_FILES['head_image']);
    $data['head_image']=$path;
    $data['password']=md5(I('post.password'));
    if($data){
      $user_info=json_encode($user_info);
      $data['user_information']=$user_info;
      $data['reg_time']=time();
      $config=C('SITEINFO_CONFIG');
      $data['status']=$config['user_reg_stats'];
      
      if($User->add($data)){
        if($config['user_reg_stats']==3){
          //发送邮件验证
          C('LAYOUT_ON',false);
          $verification=md5(time().$_POST['user_email']);
          $pageTitle="会员注册成功";
          $this->assign('pageTitle',$pageTitle);
          $mailnInfo=array(
              'greetings'=>'尊敬的会员',
              'verification'=>$verification,
              "time"=>time(),
              'id'=>$_POST['user_email'],
            );
          $this->assign('mail',$mailnInfo);
          $body=$this->fetch("Public:mail");        
          $mailserver=C('SendMessage')['host_name'];
          $mailport=C('SendMessage')['port'];
          $smtppuser=C('SendMessage')['user'];
          $smtppass=C('SendMessage')['pass'];
          $smtpMail=C('SendMessage')['from'];
          $mailType=strtoupper(C('SendMessage')['bodytype']);
          $mail=new \Org\Util\smtp($mailserver,$mailport,$smtppuser,$smtppass,true);
          $mail->debug = false; 
          if($mail->sendmail($_POST['user_email'], $smtpMail, "欢迎注册13论坛会员", $body, $mailType)){
            $Veri=M('Verification');
            $Veri->verification=$verification;
            $Veri->add_time=time();
            // $data['add_time']=time();
            if(!$Veri->add()){
              $log=new \Think\Log();
              $Log::write("在保存邮件验证码时产生了错误：".$verification->getLastSql());
              throw new Exception("验证码插入失败，请联系管理员手动审核");
            }
          }else{
            $Log::write("在给注册会员发送验证邮件时发生了错误");
          }
        }
      
      //增加积分
        $map=array(
          'user_email'=>$data['user_email']);
      $rows=$User->where($map)->find();
      AddUserIntegral($rows["id"],$config["user_reg_integral"],"新用户注册积分");
       // session('user_email',$data['user_email']);
      S('user_email',$data['user_email']);
        $this->success("用户注册成功",__CONTROLLER__."/login");

    }else{
      $Log::write("用户注册出错：".$User->getLastSql());
        throw new Exception("数据插入失败，请联系管理员");
    }
  }else{
    $this->error($User->getError());
  }
}

 public function login(){
  $pageTitle="用户中心";
  // $user_email=I('session.user_email');
  $user_email=S('user_email');
  $status="";
  $map=array(
    'user_email'=>$user_email
    );
  $User=M("user");
  $row=$User->where($map)->find();
  if($row['status']=="3"){
    $status="待激活，请尽快查看邮件激活";
  }else{
    $status="已通过邮箱验证";
  }
  $this->assign('pageTitle',$pageTitle);
  $this->assign('reg_time',date('Y:m:d',$row['reg_time']));
  $this->assign('username',$row['username']);
  $this->assign('status',$status);
  $this->assign('user_email',$user_email);
  $this->display();

 }

 public function verification(){
    $code=I('get.str');
    $user_email=I('get.id');
    $verification=M('verification');
    $map=array(
       'verification'=>$code,
       'user_email'=>$user_email
      );
    $row=$verification->where($map['verification'])->find();
    if($row !== ""){
      $User=M('user');
      $data['status']=1;
      if($User->where($map['user_email'])->save($data) != false){
        $this->success('恭喜你，账号激活成功.5秒后将跳转至用户中心',__CONTROLLER__."/login",5);
      }else{
         $this->error('用户状态已经更改，您的账号已经激活，请勿重复激活',__CONTROLLER__."/register");
      }
    }else{
      $this->error('邮件激活失败，请重新注册',__CONTROLLER__."/register",5);
    }  
 }

 public function forgetpassword(){
    $this->display();
 }

 public function emailfind(){
  if(!$this->check_verify($_POST["verify"])){
    $array=json_encode(array('data'=>$_POST['verify'],'info'=>"验证码出错",'status'=>0));
    $this->ajaxReturn($array);
  }
  $email=I('post.reg_email');
  $User=M('user');
  $map=array(
     'user_email'=>$email
    );
  $rows=$User->where($map)->find();
  if($rows==""){
    $array=json_encode(array('data'=>$email,'info'=>"您的邮箱未注册，请先注册",'status'=>0));
    $this->ajaxReturn($array);
  }else{
    // session('reg_email',$email);
    S('reg_email',$email);
    $array=json_encode(array('data'=>$email,'info'=>"邮箱正确",'status'=>1));
    $this->ajaxReturn($array);
  }
 }
  

 public function newpasswordpost(){
      // $code_id=I('session.code_id');
      $code_id=S('code_id');
      $map=array('id'=>$code_id);
      $code=I('post.code');
      $Code=M("code");
      $row=$Code->where($map)->find();
      if($row['code']==$code){
        $mess="1";
        $this->assign('mess',$mess);
      }elseif($row['code'] !=$code && $row['code'] !=""){
        $mess="2";
        $this->assign('mess',$mess);
      }else{
         $mess="0";
         $this->success('',__WEBSITE__);
      }
      $this->display();
      // $row=$User->where($map)->data($pwd)->save();
      // if($row !=""){
      //   $array=array('info'=>'密码重置成功','status'=>1);
      //   $this->ajaxReturn($array);
      // }else{
      //   $array=array('info'=>'密码重置失败','status'=>0);
      //   $this->ajaxReturn($array);
      // }

 }

 public function newpassword(){
     // $reg_email=I('session.reg_email');
      $reg_email=S('reg_email');
     $pwd['password']=md5(I('post.pwd'));
      $User=M('user');
      $map=array(
         'user_email'=>$reg_email,
        );
       $row=$User->where($map)->save($pwd);
      if($row !=""){
        $array=json_encode(array('data'=>$row,'info'=>'密码重置成功','status'=>1));
        $this->ajaxReturn($array);
      }else{
        $array=json_encode(array('data'=>$row,'info'=>'密码重置失败','status'=>0));
        $this->ajaxReturn($array);
      }
 }

  public function goback(){
    $this->display();
  }

 public function changepasswdpost(){
    $code_pre=I("post.code_pre");
    // $reg_email=I('session.reg_email');
    $reg_email=S('reg_email');
    $Code=M('code');
    $data=array(
        'code'=>$code_pre,
        'addtime'=>time()
      );
    $row=$Code->add($data);
    // session('code_id',$row);
    S('code_id',$row);

    $pageTitle="会员注册成功";
          $this->assign('pageTitle',$pageTitle);
          $mailnInfo=array(
              'greetings'=>'尊敬的会员',
              'code_pre'=>$code_pre,
              "time"=>time(),
            );
          $this->assign('mail',$mailnInfo);
         $body=$this->fetch("Public:passwdfind");      
          $mailserver=C('SendMessage')['host_name'];
          $mailport=C('SendMessage')['port'];
          $smtppuser=C('SendMessage')['user'];
          $smtppass=C('SendMessage')['pass'];
          $smtpMail=C('SendMessage')['from'];
          $mailType=strtoupper(C('SendMessage')['bodytype']);
          $mail=new \Org\Util\smtp($mailserver,$mailport,$smtppuser,$smtppass,true);
          $mail->debug = false; 
       if($mail->sendmail($reg_email, $smtpMail, "找回密码的激活码", $body, $mailType)){
      $array=json_encode(array('info'=>'邮件发送成功','status'=>1));
        $this->ajaxReturn($array);
    }

 }

 public function userinfo(){
    $User=M('user');
    // $user_email=I('cookie.user_email');
    $user_email=S('user_email');
    $map=array(
      'user_email'=>$user_email,
      );
    $rows=$User->where($map)->find();
    $this->assign('username',$rows['username']);
    $this->assign('head_img',$rows['head_image']);
    if($rows['gender']==0){
      $gender='男';
    }else{
      $gender='女';
    }
    $this->assign('gender',$gender);
    $this->display();
 }

 public function userinfopost(){
  $User=M('user');
  // $user_email=I('cookie.user_email');
  $user_email=S('user_email');
   $map=array(
      'user_email'=>$user_email,
      );
   $rows=$User->where($map)->find();
  $username=I('post.username');
    $headimg=$_FILES["headimg"];
    $sex=I('post.sex');
    if($username !="" && $sex !=""){
        if($headimg !=""){
           unlink($rows['head_image']);
        }
       $Newpath=HeadImg($headimg);
       $data['username']=$username;
       $data['gender']=$sex;
       $data['head_image']=$Newpath;
       $rows=$User->where($map)->save($data);
       if($rows!=""){
        $this->success('信息修改成功','/Index.php',3);
      }else{
      $this->error('信息修改失败','/Index.php/Home/User/userinfo',3);
    }
    }else{
      $this->error('请正确提交数据','/Index.php/Home/User/userinfo',3);
    }
 }

 public function logout(){
    if(S('user_email',null)==null){
      $this->success('返回首页','/Index.php',3);
    }else{
      $this->error("操作失败");
    }
 }

}

 ?>