<?php 
namespace Home\Controller;
use Think\Controller;
class PostTopicController extends Controller{
	public function index(){
		$forum_id=$_GET['id'];
		$this->assign('forum_id',$forum_id);
		$Category=M('category');
		$map=array(
           'id'=>$_GET['id']
			);
		$rows=$Category->where($map)->find();
		$parent_id=$rows['parent_id'];
		$map=array(
           'id'=>$parent_id
			);
		$parent_title=$Category->where($map)->find();
		$this->assign('parent_title',$parent_title['category_title']);
		$this->assign('pageTitle',$rows['category_title']);
		$this->display();
	}

    //处理ueditor提交的数据
	public function postcontent(){
		$title=I('post.title');
		$content=I('post.content');
		$category_id=I('get.id');
        $content=htmlspecialchars_decode(html_entity_decode($content));
        $time=time();
        $user_email=S('user_email');
        $User=D('user');
        $map=array(
        	'user_email'=>$user_email 
        	);
        $rows=$User->where($map)->find();
        $add_user=$rows['id'];
        $add_ip=get_client_ip();
        
        
  //       var_dump(htmlspecialchars_decode(html_entity_decode($content)))."<br>";
		// var_dump($title)."<br>";
		$Topic=D('topic');
		$data=array(
			'title'=>$title,
			'content'=>$content,
			'category_id'=>$category_id,
			'add_user'=>$add_user,
			'add_time'=>$time,
			'add_ip'=>$add_ip,
			'stats'=>1
			);
		$rows=$Topic->data($data)->add();
		// $url=U('/Home/Content/index',array('id'=>$category_id,'p'=>$rows),'html','');
		$url2='http://www.13.com/thread-'.$category_id."-".$rows.".html";
		if($rows != ""||$rows !=null){
          $this->success('页面跳转...',$url2);
		}else{
			$this->error();
		}
		$this->display();

	}
}


 ?>