<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php if($pageTitle != ''): echo ($pageTitle); ?>
	<?php else: ?>默认首页<?php endif; ?></title>
   <link rel="stylesheet" type="text/css" href="http://www.13.com/Public/Resource/Css/default.css">
   <script type="text/javascript" src="http://www.13.com/Public/Resource/Js/jquery.js"></script>
      <script type="text/javascript" src="http://www.13.com/Public/Resource/Js/main.js"></script>

</head>
<body>
	<div class="nav">
	  <ul>
		<?php $_result=Nav();if(is_array($_result)): $i = 0; $__LIST__ = $_result;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li><a href="<?php echo (CategoryURL($vo["id"])); ?>"><?php echo ($vo["category_title"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
		</ul>
	</div>
	<!--导航栏结束 -->
	<div class="logo">
		<h1><a href="http://www.13.com">在线论坛</a></h1>
		<p><em>PHPMVC在线支持论坛</em><a href="http://www.13.com">http://www.13.com</a></p>
	</div>
	<!--logo 结束 -->
  <div id="page">
    <div id="content"><div id="register">
  <p style="font-size: 18px">【用户注册】</p>
  <br>
  <span style="font-size: 15px;margin-left:10px">提示:如果你已经有账号，可以直接在右边登陆窗口进行登陆操作。</span>
  <br>
  	<form id="register-form" name="register-form" action="/index.php/Home/User/registerpost" method="post" enctype="multipart/form-data">
  	<fieldset style="margin-left: 10px;margin-top: 10px;">
  	  <legend>基础选项</legend>
  		<ul>
  			<li>登陆邮箱:<input type="text" name="user_email" id="user_email" size="25"></li>
  			<br>
  			<li>登陆密码:<input type="password" name="password" id="password" size="25"></li>
  			<br>
  			<li>确认密码:<input type="password" name="pwd-check" size="25"></li>
  			<br>
  			<li>用户名:<input type="text" name="username" size="25"></li>
  			<br>
  			<li>验证码:<input type="text" name="verifynum" size="25">&nbsp;<img src="/index.php/Home/User/verify/" id="verifyimg" ></li>
  			<div id="more-detail" style="display: none">
  			<br>
  			 <li>用户昵称:<input type="text" name="user_nickname" size="25"></li>
  			<br>
  			<li>性别:<input type="radio" name="gender" value="man">男&nbsp;<input type="radio" name="gender" value="woman">女</li>
  			<br>	
  			<li>用户头像:<input type="file" name="head_image" size="20" ><span>图片最大2M</span></li>
  			<br>
  			<li>微博地址:<input type="text" name="weibo" size="25"></li>
  			<br>
  			<li>QQ号:<input type="text" name="qq" size="25"></li>
  			<br>
  			<li>安全问题:
  			<select name="security_password">
  			<?php if(is_array($security_list)): $i = 0; $__LIST__ = $security_list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["password"]); ?>"><?php echo ($vo["password"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>	 
  			</select></li>
  			<br>	
  			<li>安全问题答案:<input type="text" name="security_answer" size="25"></li>
  			<br>		
  			</div>
  		</ul>
  </fieldset>
     <span style="font-size: 16px;margin-left: 10px;"><input type="checkbox" name="more" id="more" onchange=show(this)>高级选项</span>
     <br>
     <br>
     <input type="submit" name="sub" id="sub" value="同意条款并注册" style="margin-left: 10px;">&nbsp;<span id="detail"><a href="#" style="color: black;">阅读条款</a></span>
  	</form>
  	<div id="contest" style="display: none;width: 400px;position:absolute;background:#FFF;border:solid 1px #6e8bde;z-index: 9999;">
  	  <p>【首部及导言】
　　欢迎您使用腾讯的服务！
　　为使用腾讯的服务，您应当阅读并遵守《腾讯服务协议》（以下简称“本协议”）和《QQ号码规则》。请您务必审慎阅读、充分理解各条款内容，特别是免除或者限制责任的条款、管辖与法律适用条款，以及开通或使用某项服务的单独协议。限制、免责条款可能以黑体加粗或加下划线的形式提示您重点注意。除非您已阅读并接受本协议所有条款，否则您无权使用腾讯提供的服务。您使用腾讯的服务即视为您已阅读并同意上述协议的约束。</p>
      <p>如果您未满18周岁，请在法定监护人的陪同下阅读本协议，并特别注意未成年人使用条款。</p>
  	</div> 
</div>
<script type="text/javascript">
	function show(obj){
		$("#more-detail").toggle();
	}
    $(function(){
    	$("#detail").click(function(){
    		var windowwidth=$(window).width();
    		var windowheight=$(window).height();
    		var height=$("#contest").height();
    		var width=$("#contest").width();
    		$("#contest").css({"position":"absolute"}).animate({left:windowwidth/2-height/2,top:windowheight/2-height/2,opacity:"show"},"slow").fadeOut(10000);
    		
    	})

    	$("#verifyimg").click(function(){
    		var imgsrc=$("#verifyimg").attr('src');
    	})
    })

    

</script></div>
    <div id="sidebar">
     <div id="sidebar">
   <div id="login">
   	<p>会员中心</p>
     <div id="login-form">
     <div id="minicard"></div>
     <!-- <?php if(empty($_COOKIE['user_email'])): ?><h1>有<h1>
      <?php else: ?>
       <h1>没</h1><?php endif; ?> -->
      <?php if(!empty($_COOKIE['user_email'])): ?><script type="text/javascript">
         $("#minicard").load("http://www.13.com/index.php/Home/User/userminicard");
       </script><?php endif; ?>
      <?php if(empty($_COOKIE['user_email'])): ?><form name="login" id="userloginform" method="post" onsubmit="return userlogin() ">
            <h1> 会员登陆</h1>
             <span>邮箱:<input type="text" name="email" size="20" id="email"/></span><br>
             <br>
            <span>密码:<input type="password" name="password" id="password" size="20"></span><br>
            <br>
             <span>验证码:<input type="text" name="Verification" id="Verification" size="5">&nbsp;<img src="http://www.13.com/index.php/Home/User/verify/" alt="验证码" title="点击刷新" id="verifyimg" />
             <br>
             <input type="submit" name="submit" value="登陆">&nbsp;<a href="#" onclick="toLogin()"><img src="http://www.13.com/Public/Resource/Image/Connect_logo_7.png" width="63px" height="24px" style="margin-top: 8px"></a><span><a href="http://www.13.com/index.php/Home/user/register">[注册新用户]</a></span>&nbsp;<span><a href="http://www.13.com/index.php/Home/user/forgetpassword">[忘记密码]</a></span>
        </form><?php endif; ?>
     </div> 
   </div>
   <div id="latest-reply">
   	<p>最新回复</p>
   </div>
   <div id='rate'>
   	<p>用户排行榜</p>
   </div>
	
</div>
<script type="text/javascript">
   var verifyimg=$("#verifyimg").attr("src");  
   $("#verifyimg").click(function(){
     $(this).attr('src',verifyimg+'&random='+Math.random());
   });
</script>

<!-- <script type="text/javascript">
   var email=$("#email").attr('value');
   $("#email").focus(function(){
      $(this).attr('value',"");
   });
   $("#email").blur(function(){
      $(this).attr('value',email);
   })
</script> -->
<script type="text/javascript">
  function userlogin(obj){
               var email=$("#email").val();
               var password=$("#password").val();
               var verify=$("#Verification").val();
                $.ajax({
             url: "http://www.13.com/index.php/Home/user/userlogin",  
             type: "POST",
             data:{email:email,password:password,verification:verify},
             dataType: "json",
             error: function(){  
                 alert('Error loading XML document');  
             },  
             success: function(data){ 
                var obj=eval('('+data+')');
                if(obj.status==1){
                  //登陆成功
                  $("#userloginform").remove();
                  $("#minicard").load("http://www.13.com/index.php/Home/user/userminicard");
                }else{
                  //登陆失败
                  alert(obj.info);
                }

             }
        });
                return false;
}


</script>



    </div>
  </div>
   <!-- 内容结束-->
  <div id="footer">
  		<p>Copyright (c) 2012 BeautySoft  by <a href="http://beauty-soft.com/book/php_mvc">beauty-soft.com</a>.</p>
  </div>
</body>
</html>