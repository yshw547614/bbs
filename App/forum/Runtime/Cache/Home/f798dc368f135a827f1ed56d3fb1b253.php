<?php if (!defined('THINK_PATH')) exit();?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>
<?php if($pageTitle != ''): echo ($pageTitle); ?>
<?php else: ?> 邮件<?php endif; ?>
</title>
<style>

</style>

</head>
<body>
	<div class="main">
		<p><?php echo ($mail["greetings"]); ?></p>
		<p>您正在通过邮箱找回密码,如不是您所操作的，请忽略。</p>
		<p>验证码为<span style="color: red"><?php echo ($mail["code_pre"]); ?></span>,请您完成验证。</p>
		<p><?php echo (date($mail["time"],"Y-m-d H:i:s")); ?></p>
		<div class="url">
			本邮件由【<a href="http://www.13.com/index.php/Home" target="_blank" title="13论坛">13论坛</a>】系统自动发送，请不要直接回复。本邮件为HTML格式邮件，如果您不能阅读邮件内容，请直接以网页的形式进行浏览。
		</div>
	</div>
</body>
</html>