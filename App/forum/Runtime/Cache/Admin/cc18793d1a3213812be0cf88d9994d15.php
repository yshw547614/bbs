<?php if (!defined('THINK_PATH')) exit();?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo C('WEBTITLE');?>-<?php echo GetCookieNav();?> </title>
	<link rel="stylesheet" type="text/css" href="http://www.13.com/Public/Resource/Css/admin.css">
</head>
<body>
    <!-- 开头 -->
	<div class="header">
	  <div class="logo">
	  	<span ><h2>13在线论坛</h2></span>
	  	<span><a href="www.13.com">www.13.com</a></span>
	  </div>
	  <div class="nav">
	    <div class="nav_list">
	      <span><ul>
	      <?php $_result=GetNavAdmin();if(is_array($_result)): $i = 0; $__LIST__ = $_result;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li><a href="http://www.13.com/index-forum.php/Admin/<?php echo ($vo["category_path"]); ?>"><?php echo ($vo["category_title"]); ?></a> |</li><?php endforeach; endif; else: echo "" ;endif; ?>
	      </ul>
         </span>
	  	<div class="nav_admin_card">
	  	   <!-- 管理员信息 -->
	  	   <span>您好,<?php echo GetAdminUser();?>[<a href="#">退出</a>]&nbsp;&nbsp;<a href="#">站点退出</a></span>
	  	</div>
	  	</div>
	  	<div class="nav_search">
	  		<div style="float: left;">
	  			<!-- 面包屑导航 -->
	  			<p style="font-size:13px;">&nbsp;&nbsp;<?php echo GetCookieNav();?></p>
	  		</div>
	  		<div class="search_input">
	  		   <!-- 管理页搜索框 -->
	  		    <form>
	  			<input type="text" name="search" size="20">
	  			<input type="submit" name="sub" value="搜索">
	  			</form>
	  			&nbsp;<span id="map" style="float:left;margin-left: 20px;font-size: 15px;margin-top: 5px;">MAP</span>

	  		</div>
	  	</div>
	  </div>
	</div>
	<!-- 侧边栏 -->
	<div class="sidebar">	
	 <ul>
	 <?php $_result=GetAdminSubNav();if(is_array($_result)): $i = 0; $__LIST__ = $_result;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li><a href="http://www.13.com/index-forum.php/Admin/<?php echo ($vo["category_path"]); ?>"><?php echo ($vo["category_title"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
	 </ul>  
	</div>
	<!-- 内容栏 -->
	<div class="detail">
	 <div class="detail-content">
	  <div>
	<h2>站点信息</h2>
	<form name="webdetail" method="post">
		<P>站点信息:</P>
		<input type="text" name="webtitle" size="30" value="<?php echo C('WEBTITLE');?>"> &nbsp;&nbsp;&nbsp;<span style="color:grey">站点名称，将显示在浏览器窗口标题等位置</span>
		<P>网站名称:</P>
		<input type="text" name="webname" size="30" value="13论坛"> &nbsp;&nbsp;&nbsp;<span style="color:grey">网站名称，将显示在页面底部的联系方式处</span>
		<P>网站URL:</P>
		<input type="text" name="webname" size="30" value="http://www.13.com/index-forum.php"> &nbsp;&nbsp;&nbsp;<span style="color:grey">网站 URL，将作为链接显示在页面底部</span>
		<P>网站备案信息:</P>
		<input type="text" name="beian" size="30" value=""> &nbsp;&nbsp;&nbsp;<span style="color:grey">页面底部可以显示 ICP 备案信息，如果网站已备案，在此输入您的授权码，它将显示在页面底部，如果没有请留空</span>
		<P>网站第三方统计代码:</P>
		<textarea type="text" name="tongji" rows="5" cols="24" value=""></textarea> &nbsp;&nbsp;&nbsp;<span style="color:grey">页面底部可以显示第三方统计</span>
		<br>
		<P>关闭站点</P>
		<input type="radio" name="webopen" checked="checked" value="1">否&nbsp;&nbsp;<input type="radio" name="webopen" value="0">是
		<br>
		<br>
		<input type="submit" name="sub" name="提交">
	</form>
</div>
	  </div>
	</div>
	
</body>
</html>
<script type="text/javascript" src="http://www.13.com/Public/Resource/Js/jquery.js"></script>
	<script type="text/javascript" src="http://www.13.com/Public/Resource/Js/main.js"></script>
<!-- <script type="text/javascript">
	$(document).ready(function(){
		$(".sidebar #path").click(function(){
			$path=$(this).attr('href');
			// alert($path);
		    $element=$path.split('/');
		    $Newpath=$element[8];
		    // alert($Newpath);
		    $CF=$Newpath.split('-');
		    $Controller=$CF[0];
		    $func=$CF[1];
		    // alert($Controller);
		    // alert($func);
		    $url="http://www.13.com/index-forum.php/Admin"+"/"+$Controller+"/"+$func;
		    alert($url);
		     // $("#serverinfo").remove();
		    $(".Cont").load("/App/forum/Admin/View/Setting/basic.html");
		    // $(".Cont").load("");
		})

		
	})
</script> -->