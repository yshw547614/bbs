<?php if (!defined('THINK_PATH')) exit();?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>管理员后台</title>
	<link rel="stylesheet" type="text/css" href="http://www.13.com/Public/Resource/Css/admin.css">
</head>
<body>
    <!-- 开头 -->
	<div class="header">
	  <div class="logo">
	  	<span ><h2>13在线论坛</h2></span>
	  	<span><a href="www.13.com">www.13.com</a></span>
	  </div>
	  <div class="nav">
	    <div class="nav_list">
	      <span><ul>
	      <?php if(is_array($nav)): $i = 0; $__LIST__ = $nav;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li id="<?php echo ($vo["id"]); ?>"><a href="/index.php/Admin/Index/index/id/<?php echo ($vo["id"]); ?>"><?php echo ($vo["category_title"]); ?></a> |</li><?php endforeach; endif; else: echo "" ;endif; ?>
	      </ul>
         </span>
	  	<div class="nav_admin_card">
	  	   <!-- 管理员信息 -->
	  	   <span>您好,<?php echo ($username); ?>[<a href="#">退出</a>]&nbsp;&nbsp;<a href="#">站点退出</a></span>
	  	</div>
	  	</div>
	  	<div class="nav_search">
	  		<div style="float: left;">
	  			<!-- 面包屑导航 -->
	  			<p>111</p>
	  		</div>
	  		<div class="search_input">
	  		   <!-- 管理页搜索框 -->
	  		    <form>
	  			<input type="text" name="search" size="20">
	  			<input type="submit" name="sub" value="搜索">
	  			</form>
	  			&nbsp;<span id="map" style="float:left;margin-left: 20px;font-size: 15px;margin-top: 5px;">MAP</span>

	  		</div>
	  	</div>
	  </div>
	  
   


	</div>
	<!-- 侧边栏 -->
	<div class="sidebar">	
	 <ul>
	 <?php if(is_array($subnav)): $i = 0; $__LIST__ = $subnav;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li><a href=""><?php echo ($vo["category_title"]); ?></a></li><?php endforeach; endif; else: echo "" ;endif; ?>
	 </ul>  
	</div>
	<!-- 内容栏 -->
	<div class="detail">
	  <div>
	  	<span><h2>系统信息</h2></span>
        <span>
        <p>服务器系统及PHP: &nbsp;&nbsp;<?php echo PHP_OS ?>/PHP<?php echo PHP_VERSION?></p>
        </span>
        <span><p>服务器软件:<?php echo $_SERVER['SERVER_SOFTWARE']?></p></span>
        <span><p>服务器mysql 版本:<?php echo mysql_get_server_info()?></p></span>
        <span><p>上传许可:<?php echo get_cfg_var('upload_max_filesize')?></p></span>
        <span><p>当前数据库尺寸:</p></span>
	  </div>
	</div>
	
</body>
</html>
<script type="text/javascript" src="http://www.13.com/Public/Resource/Js/juqery.js"></script>
<script type="text/javascript" src="http://www.13.com/Public/Resource/Js/main.js"></script>
<script type="text/javascript">
	
</script>