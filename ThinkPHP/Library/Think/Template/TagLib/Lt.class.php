<?php 
namespace Think\Template\TagLib;
use Think\Template\TagLib;
class Lt extends TagLib{
	//创建一个lt标签，用来加载百度的ueditor，这样就不用每次都是用javascript，每次都设置
	protected $tags=array(
       'editor'=>array('attr'=>'id,url,width,height,name','close'=>1)
		);

	public function _editor($tag,$content){
		$id=$tag['id'];
		$url=$tag['url'];
		$width=$tag['width'];
		$height=$tag['height'];
    $name=$tag['name'];
    $editor_url=$url."/Resource/Ueditor/";
    // $serverUrl=$url."/index.php/Home/UploadFile/index";
		$str="
          <editor id='".$id."' url='".$url."' name='".$name."' width='".$width."' height='".$height."' />
          <script id='".$id."' type='text/plain' style='width:".$width.";height:".$height."'/>
          ".$content." 
          这里是初始化内容
          </script>
          <script type='text/javascript' src='{$editor_url}ueditor.config.js'>
          </script>
          <script type='text/javascript' src='{$editor_url}ueditor.all.js'>
          </script>
          <script type='text/javascript'>
            UE.getEditor('".$id."',{
            	toolbars:[
            	['fullscreen','source','undo','redo','bold','italic','underline','fontborder','|','superscript','|','fontsize','simpleupload','link','emotion']
            	],
            	autoHeightEnabled:false,
            	autoFloatEnabled:false,
            	initialContent:'你好，请在此处发表你的内容',
            	initialFrameWidth:'".$width."',
            	initialFrameHeight:'".$height."',
            	saveInterval:600,
            	wordCountMsg:'你已经输入了{#count}个字符，还剩下{#leave}个字符可以输入',
            });
            
          </script>
		";
				return $str;
	}
}




 ?>